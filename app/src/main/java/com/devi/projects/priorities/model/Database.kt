package com.devi.projects.priorities.model

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = arrayOf(Task::class), version = 1)
abstract class Database: RoomDatabase() {
    abstract fun taskDao(): TaskDao
}
package com.devi.projects.priorities.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.persistence.room.Room
import android.support.v7.app.AppCompatActivity
import com.devi.projects.priorities.model.Database
import com.devi.projects.priorities.viewmodel.task.TaskListViewModel

class ViewModelFactory( private val activity: AppCompatActivity ): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TaskListViewModel::class.java)) {
            val db = Room.databaseBuilder(activity.applicationContext, Database::class.java, "posts").build()
            @Suppress("UNCHECKED_CAST")
            return TaskListViewModel(db.taskDao()) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")

    }
}
package com.devi.projects.priorities.viewmodel.task

import android.arch.lifecycle.MutableLiveData
import android.view.View
import com.devi.projects.priorities.R
import com.devi.projects.priorities.model.Task
import com.devi.projects.priorities.model.TaskDao
import com.devi.projects.priorities.network.PriorityApi
import com.devi.projects.priorities.viewmodel.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.Observable
import javax.inject.Inject

class TaskListViewModel(private val taskDao: TaskDao) : BaseViewModel() {

    @Inject
    lateinit var priorityApi: PriorityApi
    val taskListAdapter: TaskListAdapter = TaskListAdapter()

    private lateinit var subscription: Disposable

    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadTasks() }
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()

    init {
        loadTasks()
    }

    fun refreshTasks() {
        loadTasks()
    }

    private fun loadTasks() {
        subscription = Observable.fromCallable { taskDao.all }
            .concatMap { dbTaskList ->
                priorityApi.getTasks().concatMap { apiTaskList ->
                    taskDao.updateTasks(apiTaskList)
                    if (dbTaskList.isEmpty()) {
                        Observable.just(apiTaskList)
                    } else {
                        Observable.just(dbTaskList)
                    }
                }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onStart() }
            .doOnTerminate { onFinish() }
            .subscribe(
                { result -> onSuccess(result) },
                { onError() }
            )
    }

    private fun getTasks(): Observable<List<Task>> {
        return Observable.fromCallable { taskDao.all }
            .concatMap { dbPostList ->
                priorityApi.getTasks().concatMap { apiPostList ->
                    taskDao.updateTasks(apiPostList)
                    Observable.just(apiPostList)
                }
                Observable.just(dbPostList)
            }
    }

    private fun onStart() {
        errorMessage.value = null
        loadingVisibility.value = View.VISIBLE
    }

    private fun onFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onSuccess(taskList: List<Task>) {
        taskListAdapter.updateTaskList(taskList)
    }

    private fun onError() {
        errorMessage.value = R.string.error_post
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}
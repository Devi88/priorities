package com.devi.projects.priorities.viewmodel

import com.devi.projects.priorities.network.NetworkModule
import com.devi.projects.priorities.viewmodel.task.TaskListViewModel
import com.devi.projects.priorities.viewmodel.task.TaskViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(NetworkModule::class)])

interface ViewModelComponent {
    fun inject(taskListViewModel: TaskListViewModel)

    fun inject(taskViewModel: TaskViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelComponent

        fun networkModule(networkModule: NetworkModule): Builder
    }
}
package com.devi.projects.priorities.viewmodel

import android.arch.lifecycle.ViewModel
import com.devi.projects.priorities.network.NetworkModule
import com.devi.projects.priorities.viewmodel.task.TaskListViewModel
import com.devi.projects.priorities.viewmodel.task.TaskViewModel

abstract class BaseViewModel : ViewModel() {
    private val component: ViewModelComponent = DaggerViewModelComponent
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is TaskListViewModel -> component.inject(this)
            is TaskViewModel -> component.inject(this)
        }
    }
}
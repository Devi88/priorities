package com.devi.projects.priorities.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Task(
    @field:PrimaryKey
    val id: Int,
    val title: String,
    val position: Int
)
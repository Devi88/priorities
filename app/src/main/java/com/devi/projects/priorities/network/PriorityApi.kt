package com.devi.projects.priorities.network

import com.devi.projects.priorities.model.Task
import retrofit2.http.GET
import io.reactivex.Observable

interface PriorityApi {
    @GET("/api/tasks")
    fun getTasks(): Observable<List<Task>>
}
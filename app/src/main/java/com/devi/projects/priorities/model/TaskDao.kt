package com.devi.projects.priorities.model

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction

@Dao
interface TaskDao {
    @get:Query("Select * FROM task")
    val all: List<Task>

    @Insert
    fun insertAll(tasks: List<Task>)

    @Query("DELETE FROM task")
    fun deleteAll()

    @Transaction
    fun updateTasks(tasks: List<Task> ) {
        deleteAll()
        insertAll(tasks)
    }
}
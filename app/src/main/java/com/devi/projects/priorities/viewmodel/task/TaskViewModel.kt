package com.devi.projects.priorities.viewmodel.task

import android.arch.lifecycle.MutableLiveData
import com.devi.projects.priorities.model.Task
import com.devi.projects.priorities.viewmodel.BaseViewModel

class TaskViewModel : BaseViewModel() {

    private val title = MutableLiveData<String>()

    fun bind(task: Task) {
        title.value = task.title
    }

    fun getTitle(): MutableLiveData<String> {
        return title
    }
}
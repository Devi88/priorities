package com.devi.projects.priorities.viewmodel.task

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.devi.projects.priorities.R
import com.devi.projects.priorities.databinding.ItemTaskBinding
import com.devi.projects.priorities.model.Task

class TaskListAdapter: RecyclerView.Adapter<TaskListAdapter.ViewHolder>() {
    private lateinit var taskList: List<Task>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskListAdapter.ViewHolder {
        val binding: ItemTaskBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_task, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TaskListAdapter.ViewHolder, position: Int) {
        holder.bind(taskList[position])
    }

    override fun getItemCount(): Int {
        return if(::taskList.isInitialized) taskList.size else 0
    }

    fun updateTaskList(taskList:List<Task>){
        this.taskList = taskList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemTaskBinding):RecyclerView.ViewHolder(binding.root){
        private val viewModel = TaskViewModel()

        fun bind(task: Task){
            viewModel.bind(task)
            binding.viewModel = viewModel
        }
    }
}